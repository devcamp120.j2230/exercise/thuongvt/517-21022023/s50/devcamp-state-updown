import { Component } from "react";

class Updown extends Component {
    constructor (props){
        super(props);
        this.state ={
            count: 0
        }
    }

    clickUpChanghandler = ()=>{
        this.setState ({
            count: this.state.count + 1
        })
    }

    clickDownChanghandler = ()=>{
        this.setState ({
            count: this.state.count -1
        })
    }

    render() {
        return (
            <>
                <div className="row mt-4" style={{backgroundColor:"Highlight"}}>
                <p style={{color:"white", textAlign:"center"}}>Count: {this.state.count}</p>
                </div>
                <div className="row mt-3">
                    <button className="btn btn-success" onClick={this.clickUpChanghandler}>Tăng lên</button>
                </div>
                <div className="row mt-3">
                    <button className="btn btn-primary" onClick={this.clickDownChanghandler}>Giảm xuống</button>
                </div>
            </>
        )
    }
}

export default Updown